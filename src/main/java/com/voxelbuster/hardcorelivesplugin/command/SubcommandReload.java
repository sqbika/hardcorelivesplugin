package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubcommandReload extends PluginSubcommand {
    protected SubcommandReload(HardcoreLivesPlugin plugin) {
        super("reload", plugin);
        this.description = "Reloads the plugin and player data from the config.";
        this.usage = "/hl reload";
        this.aliases = Collections.singletonList("reload");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        if (PermissionUtil.hasPermission(sender, "hardcorelives.reload")) {
            if (aliases.contains(alias.toLowerCase())) {
                if (args.length < 1) {
                    plugin.reload(false);
                } else {
                    plugin.reload(Boolean.parseBoolean(args[0]) || args[0].equals("1"));
                }

                sender.sendMessage(ChatColor.GREEN + "HardcoreLives reload complete.");
                return true;
            }
        } else {
            sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
            return false;
        }
        sendUsageMessage(sender);
        return false;
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        return new ArrayList<>();
    }
}

package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.PermissionUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.scoreboard.DisplaySlot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SubcommandScoreboardSlot extends PluginSubcommand {
    protected SubcommandScoreboardSlot(HardcoreLivesPlugin plugin) {
        super("scoreboardSlot", plugin);
        this.description = "Set the scoreboard display slot for the plugin. This change is for every player.";
        this.usage = "/hl scoreboardSlot <SIDEBAR/BELOW_NAME/PLAYER_LIST>";
        this.aliases = Arrays.asList("scoreboardslot", "ss");
    }

    @Override
    public boolean execute(CommandSender sender, String alias, String[] args) {
        ConfigManager configManager = plugin.getConfigManager();
        if (args.length == 1) {
            if (PermissionUtil.hasPermission(sender, "hardcorelives.scoreboard.manage")) {
                try {
                    DisplaySlot ds = DisplaySlot.valueOf(args[0]);
                    configManager.getConfig().setScoreboardDisplaySlot(ds);
                    plugin.getScoreboardObjective().setDisplaySlot(ds);
                    plugin.updateScoreboard();
                    sender.sendMessage("Scoreboard slot updated.");
                } catch (IllegalArgumentException e) {
                    sender.sendMessage(ChatColor.RED + "Invalid display slot type.");
                }
                return true;
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "You do not have permission to use that command.");
                return false;
            }
        } else {
            sendUsageMessage(sender);
            return false;
        }
    }

    @Override
    public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (aliases.contains(alias.toLowerCase())) {
            if (args.length == 1) {
                ArrayList<String> optionNames = Arrays.stream(DisplaySlot.values()).map(Enum::toString)
                        .collect(Collectors.toCollection(ArrayList::new));
                return optionNames.stream().filter(s -> s.toUpperCase().startsWith(args[0]))
                        .collect(Collectors.toList());
            } else {
                return new ArrayList<>();
            }
        } else {
            return new ArrayList<>();
        }
    }
}

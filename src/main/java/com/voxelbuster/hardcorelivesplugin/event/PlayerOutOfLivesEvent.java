package com.voxelbuster.hardcorelivesplugin.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerOutOfLivesEvent extends PlayerEvent {
    private static final HandlerList handlers = new HandlerList();

    public PlayerOutOfLivesEvent(Player respawnPlayer) {
        super(respawnPlayer);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}

package com.voxelbuster.hardcorelivesplugin;

import org.bukkit.scheduler.BukkitTask;

import java.io.IOException;

public class SaveTaskScheduler {
    private final HardcoreLivesPlugin plugin;

    private final Runnable saveTask;
    private long saveInterval = 0;
    private BukkitTask currentTaskHandle;

    public SaveTaskScheduler(HardcoreLivesPlugin plugin) {
        this.plugin = plugin;

        saveTask = () -> {
            try {
                plugin.getConfigManager().saveAllPlayers(plugin.getPlayersDir());
            } catch (IOException e) {
                plugin.log.severe("Saving player data failed!");
                e.printStackTrace();
            }
        };
    }

    public void setSaveInterval(long ticks) {
        this.saveInterval = ticks;
    }

    public void rescheduleTask() {
        if (currentTaskHandle != null) {
            currentTaskHandle.cancel();
        }

        if (saveInterval > 0) {
            this.currentTaskHandle = plugin.getServer().getScheduler()
                    .runTaskTimer(plugin, saveTask, saveInterval, saveInterval);
        }
    }
}
